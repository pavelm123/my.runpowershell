﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.IO;
using System.Management.Automation.Remoting;
using System.Runtime.Remoting.Messaging;
using NLog;


//Add manually reference to: C:\Program Files (x86)\Reference Assemblies\Microsoft\WindowsPowerShell\3.0\System.Management.Automation.dll 
//Requirements: Windows PowerShell 3.0.
//TODO: READ! Writing Host application: https://msdn.microsoft.com/en-us/library/ee706580(v=vs.85).aspx
//http://community.bartdesmet.net/blogs/bart/archive/2008/07/06/windows-powershell-through-ironruby-writing-a-custom-pshost.aspx

//todo: should I call writer() in try/catch block??

namespace DrWorkerService.PowershellSupport
{
    /// <summary>
    /// Provides various methods to work PowerShell scripts.
    /// </summary>
    public class RunPowerShellScript
    {
        public event EventHandler<PSInvocationStateChangedEventArgs> InvocationStateChangedCompleted;
        public event EventHandler<PSInvocationStateChangedEventArgs> InvocationStateChangedFailed;

        public delegate void WriterDelegate(string s);

        private PSDataCollection<PSObject> _output = new PSDataCollection<PSObject>();


        private readonly WriterDelegate _writer;//outputs strings somewhere - to Amazon queue or console
        private readonly Logger _logger;

        public RunPowerShellScript(WriterDelegate writer, Logger logger)
        {
            _writer = writer;
            _logger = logger;
        }
        /// <summary>
        /// Loads text of the script with the specified path.
        /// </summary>
        /// <remarks>
        /// If script with the specififed path dosen't exist, then <see cref="String.Empty"/> will be returned.
        /// </remarks>
        /// <param name="pathToScript">Path to script.</param>
        /// <returns>Text of the script with the specified path.</returns>
        public string LoadScript(string pathToScript)
        {
            var text = string.Empty;
          
            if (File.Exists(pathToScript))
            {
                text = File.ReadAllText(pathToScript, Encoding.UTF8);
            }

            return text;
        }

        private string BuildErrorInfo(ErrorRecord err)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(err);
            sb.Append(err.Exception);
            sb.AppendLine(err.ScriptStackTrace);
            sb.AppendLine(err.InvocationInfo.PositionMessage);
            sb.AppendLine(err.CategoryInfo.GetMessage());
            sb.AppendLine(err.FullyQualifiedErrorId);
            if (err.ErrorDetails!= null) sb.AppendLine(err.ErrorDetails.ToString());
            return sb.ToString();
        }

        private void stream_DataAdded<T>(object sender, DataAddedEventArgs e)
        {
            PSDataCollection<T> collection = sender as PSDataCollection<T>;
            try
            {
                var item = collection[e.Index];

                if (item is ErrorRecord)
                {
                    _writer(BuildErrorInfo(collection[e.Index] as ErrorRecord));
                }
                else
                {
                    //Collection<T> results = collection.ReadAll();
                    //_writer(results.ToString());
                    _writer(item.ToString());
                }
            }
            catch (Exception ex)
            {
                _writer($"Suppressed Exception in stream_DataAdded handler:{ex}");
                _logger.Error(ex);
            }
        }



        public PSDataCollection<PSObject> RunPowershellScriptLocalyAsync(
            string scriptFile,
            IDictionary parameters)
        {
            if (string.IsNullOrEmpty(scriptFile)) { throw new ArgumentNullException("scriptFile"); }
            if (parameters == null) { throw new ArgumentNullException("parameters"); }

            //TODO: we need to import Clusters
            InitialSessionState iss = InitialSessionState.CreateDefault();
            //iss.ImportPSModule(new string[] { "C:\\windows\\system32\\windowspowershell\\v1.0\\Modules\\FailoverClusters\\FailoverClusters.psd1" });
            //iss.ImportPSModule(new string[] { "C:\\windows\\system32\\windowspowershell\\v1.0\\Modules\\FailoverClusters\\Microsoft.FailoverClusters.PowerShell.dll" });
            iss.ImportPSModule(new string[] { "FailoverClusters" });
            //to check, type in console: PS> $host.Runspace.InitialSessionState

            //RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();
            //TODO: runspaceConfiguration.Assemblies...
            // ScriptConfigurationEntry entry = new ScriptConfigurationEntry("read-mycmdlet","definition");
            //TODO: consider using runspaceConfiguration.InitializationScripts.Append(entry); instead of ImportPSModule
            //http://stackoverflow.com/questions/20399186/what-is-the-difference-between-pipeline-invoke-and-powershell-invoke

//            using (Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration))
            using (Runspace runspace = RunspaceFactory.CreateRunspace(iss))
            {
                runspace.Open();


                PowerShell powershell = PowerShell.Create(RunspaceMode.NewRunspace);
                powershell.Runspace = runspace;
                //TODO: Sometimes gives error: There is no Runspace available to run scripts in this thread. 
                //You can provide one in the DefaultRunspace property of the System.Management.Automation.Runspaces.Runspace type. 
                //The script block you attempted to invoke was: $this.ServiceName
                //powershell.AddScript("function write-host {}");
                    
                powershell.AddScript(scriptFile);
                powershell.AddParameters(parameters);
                powershell.AddCommand("out-string").AddParameter("Stream").AddParameter("width",80);

                powershell.InvocationStateChanged += delegate (object sender, PSInvocationStateChangedEventArgs e)
                {
                    _logger.Debug($"State={e.InvocationStateInfo.State}, Reason={e.InvocationStateInfo.Reason}");
                    switch (e.InvocationStateInfo.State)
                    {
                        case PSInvocationState.Completed: //object has finished the invocation of the pipeline
                            InvocationStateChangedCompleted?.Invoke(sender,e);
                            break;
                        case PSInvocationState.Failed:
                            //todo: new ErrorEventArgs((sender as PowerShell).Streams.Error.ReadAll())
                            InvocationStateChangedFailed?.Invoke(sender,e);
                            break;

                        case PSInvocationState.Disconnected:
                        case PSInvocationState.NotStarted:
                        case PSInvocationState.Stopped: //object has halted the invocation of the pipeline due to a Stop request.
                        case PSInvocationState.Stopping: //object is in the process of halting the invocation of the pipeline
                            _logger.Debug($"InvocationStateChanged: {e.InvocationStateInfo.State}");
                            break;
                    }
                };
                
                _output.DataAdded += stream_DataAdded<PSObject>;
                powershell.Streams.Debug.DataAdded += stream_DataAdded<DebugRecord>;
                powershell.Streams.Error.DataAdded += stream_DataAdded<ErrorRecord>;
                powershell.Streams.Progress.DataAdded += stream_DataAdded<ProgressRecord>;
                powershell.Streams.Verbose.DataAdded += stream_DataAdded<VerboseRecord>;
                powershell.Streams.Warning.DataAdded += stream_DataAdded<WarningRecord>;
                powershell.Streams.ClearStreams();


                //PSInvocationSettings settings = new PSInvocationSettings();
                //TODO: settings.Host = new PSHost()// TODO: consider using this
                //PSSessionConfiguration


                //returnValue = instance.BeginInvoke(input, output, settings, callback, state)
                //IAsyncResult asyncResult = BeginInvoke<TInput, TOutput>(PSDataCollection<TInput> input,
                //    PSDataCollection<TOutput> output, PSInvocationSettings settings, AsyncCallback callback, Object state )

                PSDataCollection<PSObject> psResult = null;
                try
                {
                    IAsyncResult asyncResult = powershell.BeginInvoke<PSObject, PSObject>(null, _output);

                    //TODO: change this
                    while (asyncResult.IsCompleted)
                    {
                        Thread.Sleep(1000);
                        _logger.Debug("Sleeping...");
                    }

                    asyncResult.AsyncWaitHandle.WaitOne();
                    psResult = powershell.EndInvoke(asyncResult);
                    powershell.Stop();
                    //_writer("Proxy " + runspace.SessionStateProxy.PSVariable.GetValue("Error").ToString());

                }
                catch (System.Management.Automation.RuntimeException runtimeException)
                {
                    string s = runtimeException + Environment.NewLine + BuildErrorInfo(runtimeException.ErrorRecord);
                    _logger.Error($"Runtime Exception: {s}");
                    _writer($"Runtime Exception: {s}");
                }
                catch (Exception ex)
                {
                    _logger.Error($"Suppressed errror: {ex}");
                    _writer($"Suppressed errror: {ex}");
                }

                //read: http://blogs.msdn.com/b/kebab/archive/2014/04/28/executing-powershell-scripts-from-c.aspx

                if (powershell.HadErrors) // TODO: do we still need this?
                {
                    foreach (ErrorRecord e in powershell.Streams.Error.ReadAll())
                    {
                        _writer(BuildErrorInfo(e));
                    }
                        
                }
                if (psResult!=null)
                foreach (PSObject pso in psResult)
                {
                    _writer(pso.ToString());
                    _writer(pso.BaseObject.ToString());
                    //if testing scritp has only get-service command, calling ToString() will lead to the following:
                    //Exception calling \"ToString\" with \"0\" argument(s): \"
                    //There is no Runspace available to run scripts in this thread.You can provide one in the DefaultRunspace 
                    //property of the System.Management.Automation.Runspaces.Runspace type. The script block you attempted to invoke was: $this.ServiceName
                }
                runspace.Close();
                
                return psResult;
            }
        }





        /* outdated way, but intersting because of MergeMyResults command 

            RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();

            using (Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration))
            {
                runspace.Open();
                
                Command scriptCommand = new Command(scriptFile);

                foreach (string scriptParameterKey in parameters.Keys)
                {
                    CommandParameter commandParm = new CommandParameter(scriptParameterKey, parameters[scriptParameterKey]);
                    scriptCommand.Parameters.Add(commandParm);
                }
                Pipeline pipeline = runspace.CreatePipeline();
                pipeline.Commands.Add(scriptCommand);
                
                pipeline.Commands[0].MergeMyResults(PipelineResultTypes.Error, PipelineResultTypes.Output);
                //TODO: check this: pipeline.Commands[0].MergeUnclaimedPreviousCommandResults

                Collection<PSObject> psObjects = pipeline.Invoke();
                //Or call pipeline.InvokeAsync(); and check pipeline.Output.EndOfPipeline become true
            }


        public static Collection<PSObject> RunPowershellScriptRemotelyAsync(string scriptFile, IDictionary parameters,string username, string password)
        {
            /* https://technet.microsoft.com/en-us/library/dn614664(v=vs.85).aspx
            WSManConnectionInfo connectionInfo = new WSManConnectionInfo();
            connectionInfo.OperationTimeout = 4 * 60 * 1000; // 4 minutes.
            connectionInfo.OpenTimeout = 1 * 60 * 1000; // 1 minute.
            System.Security.SecureString pwd = new System.Security.SecureString();
            foreach (char pass in password)
            {
                pwd.AppendChar(pass);
            }

            PSCredential credential = new PSCredential(username, pwd);

            then call main execution function
        }

        */

        /// <summary>
        /// Converts collection of <see cref="PSObject"/> objects to string.
        /// </summary>
        /// <param name="psObjects">Collection of <see cref="PSObject"/> objects.</param>
        /// <returns>Result string.</returns>
        public static string ToSingleString(Collection<PSObject> psObjects)
        {
            var stringBuilder = new StringBuilder();
            foreach (var psObject in psObjects)
            {
                stringBuilder.AppendLine(psObject.ToString());
            }

            return stringBuilder.ToString();
        }


        /*
            let it be here for a while - method to call PS script via starting new process:

            var startInfo = new ProcessStartInfo
                                {
                                    FileName = @"powershell.exe",
                                    Arguments = string.Format(@"& '{0}' {1}", fullPathToScript, scriptParameters),
                                    //// RedirectStandardInput = true,
                                    RedirectStandardOutput = true,
                                    RedirectStandardError = true,
                                    UseShellExecute = false,
                                    CreateNoWindow = true
                                };

            ////var directoryName = Path.GetDirectoryName(fullPathToScript);
            ////if (!string.IsNullOrEmpty(directoryName))
            ////{
            ////    startInfo.WorkingDirectory = directoryName;
            ////}

            var user = ConfigurationUtils.GetUser();
            if (!string.IsNullOrEmpty(user.Name))
            {
                startInfo.UserName = user.Name;

                var password = user.Password ?? string.Empty;
                var securePassword = new SecureString();
                foreach (var pChar in password.ToCharArray())
                {
                    securePassword.AppendChar(pChar);
                }

                startInfo.Password = securePassword;
            }

            if (!string.IsNullOrEmpty(user.Domain))
            {
                startInfo.Domain = user.Domain;
            }

            using (var process = new Process { StartInfo = startInfo })
            {
                process.Start();

                process.WaitForExit();

                var output = process.StandardOutput.ReadToEnd();
                var errors = process.StandardError.ReadToEnd();

                var viewModel = new RunScriptViewModel
                                    {
                                        Output = output,
                                        Errors = errors
                                    };

                return View(viewModel);
            }

        #endregion
         */
    }
}