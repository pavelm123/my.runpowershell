﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrWorkerService.PowershellSupport;
using NLog;

namespace runps
{
    class Program
    {
        private readonly static Logger logger = LogManager.GetCurrentClassLogger();


        private static void Main(string[] args)
        {
            if (args.Length == 0) return;
            Console.WriteLine("Running {0} script", args[0]);


            RunPowerShellScript host = new RunPowerShellScript(v => { Console.WriteLine(v); }, logger);


            string script = host.LoadScript(args[0]);
            Dictionary<string,string> pars = new Dictionary<string, string>();
            pars.Add("test","test");
            
            var result = host.RunPowershellScriptLocalyAsync(script, pars);
            if (Environment.UserInteractive) Console.ReadKey();
        }

    }
}
