﻿param
(
	[Switch]
	$FullList = $false,


	[string]$clusterName
)
process
{

Set-StrictMode -Version latest
#Import-Module “sqlps” -DisableNameChecking
#$fcmodule = Import-Module FailoverClusters -PassThru
#Import-Module ActiveDirectory
#Import-Module ServerManager
#$succeeded = Import-Module WebAdministration #now you can do 'cd IIS:\'

function write-host($p) { write-output $p }
write-host "test"


write-output $host
write-output "MachineName = $([Environment]::MachineName)"
write-output "EnvironmentVersion = $([Environment]::Version), OSVersion=$([Environment]::OSVersion), Powershell version: $($PSVersionTable.PSVersion)"
write-output "Execution policy: $(Get-ExecutionPolicy)"
write-output "User name = $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name), UserDomainName=$([Environment]::UserDomainName) \ $([Environment]::UserName)"
#Set-ExecutionPolicy RemoteSigned -Scope LocalMachine
if ($FullList)
{
    get-childitem env:\

    # get all loaded assemblies
    $dlls = [System.AppDomain]::CurrentDomain.GetAssemblies()
    Write-Output "Loaded modules:"
    $dlls | select -property Modules  | format-table
}

Get-NetIPAddress | select IPAddress, InterfaceAlias | format-table

# write-host "write-host test" # this does not work from C#
write-output "write-output test"
write-verbose "write-verbose test"
write-debug "write-debug test"
write-progress "write-progress test"
#write-warning "write-warning test" #this either
"out-default test" | out-default 
"out-host test" | out-host -paging 

try
{
    $cluster = get-cluster $clusterName
    $cluster | Get-ClusterNode | Get-ClusterResource | fl *
    $cluster | Get-ClusterQuorum | fl *
}
catch
{
    $ex = $_.Exception
    Write-Error -Message "Cannot find cluster $clusterName" -Exception $ex
}

}